import React from 'react';
import BaseTemplate from "../base-template/BaseTemplate";

type Props = {
    name: string,
    top?: React.ReactNode,
    left?: React.ReactNode,
    right?: React.ReactNode
};

const EditorTemplate = (props: Props) => {
    return (
        <BaseTemplate name={props.name}>
            <div className="label-inner">
                <div className="container">

                    <div className="label-text-head">
                        {props.top}
                    </div>

                    <div className="row">
                        <div className="col-md-8">
                            {props.left}
                        </div>

                        <div className="col-md-4">
                            {props.right}
                        </div>
                    </div>
                </div>
            </div>
        </BaseTemplate>
    );
};
export default EditorTemplate;
