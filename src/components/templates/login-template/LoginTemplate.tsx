import React from "react";
import {AuxProps} from "../../../model/interfaces/AuxProps";
import {IconType} from "../../../model/enums/icon-type";
import {IconGenerator} from "../../atoms/icons";


const loginTemplate = (props: AuxProps) => {
    return (
        <>
            <header className="headers">
                <div className="header-area">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-3 col-6 logo">
                                <IconGenerator icon={IconType.APP_LOGO}/>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <main id="content" className="page-main">
                {props.children}
            </main>

        </>

    )
};

export default loginTemplate;
