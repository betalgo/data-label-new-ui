import React from 'react';
import BaseTemplate from "../base-template/BaseTemplate";
import {AuxProps} from "../../../model/interfaces/AuxProps";

type Props = { name: string } & AuxProps;

const ProjectCreateTemplate = (props: Props) => {
    return (
        <BaseTemplate name={props.name}>

            <div className="label-inner">
                <div className="container">
                    {props.children}
                </div>
            </div>

        </BaseTemplate>
    );
};
export default ProjectCreateTemplate;
