import React from 'react';
import BaseTemplate from "../base-template/BaseTemplate";
import {AuxProps} from "../../../model/interfaces/AuxProps";

type Props = {
    name: string
} & AuxProps;

const ProjectTemplate = (props: Props) => {
    return (
        <BaseTemplate name={props.name}>
            <div className="project-area">
                    {props.children}
            </div>
        </BaseTemplate>

    );
};
export default ProjectTemplate;
