import React from 'react';
import {AuxProps} from "../../../model/interfaces/AuxProps";
import Header from "../../organisms/header";

type Props = {
    name: string
} & AuxProps;

const BaseTemplate = (props: Props) => {

    return (
        <>
            <Header name={props.name}/>

            <main id="content" className="page-main">
                {props.children}
            </main>

        </>
    );
};

export default BaseTemplate;
