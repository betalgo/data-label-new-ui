import React from "react";
import {Menu} from "../../../../model/types/ui/Menu";
import MenuItem from "../../../molecules/row/menu";

interface Props {
    list: Array<Menu>
}

const menuList = React.memo(({list}: Props) => {

    return (
        <ul className="row">
            {list.map((menu: Menu, i) => {
                return <li className="col-6"><MenuItem {...menu} /></li>
            })}
        </ul>
    )
});

export default menuList;
