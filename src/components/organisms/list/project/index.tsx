import React from 'react';
import ProjectItem from "../../../molecules/row/project";

type Props = {
    projects: Array<any>
};

const ProjectList = React.memo(({projects}: Props) => {
    return (
        <div className="row">

            {projects.map((p, i) => {
                return <div key={i} className="col-6 col-sm-3 mb-4">
                    <ProjectItem {...p} />
                </div>;
            })}

        </div>
    );
});
export default ProjectList;
