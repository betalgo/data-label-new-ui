import React from "react";
import {IconGenerator} from "../../atoms/icons";
import {IconType} from "../../../model/enums/icon-type";
import MenuList from "../list/header-menu";
import {HEADER_MENU} from "../../../model/static/HeaderMenu";
import Profile from "../../molecules/profile";

type Props = {
    name: string
}

const header = ({name}: Props) => {
    return (
        <header className="headers">

            <div className="header-area">
                <div className="container">
                    <div className="row">
                        <div className="col-md-3 col-6 logo">
                            <IconGenerator icon={IconType.APP_LOGO}/>
                        </div>

                        <div className="col-md-3 col-5 search-bar"></div>

                        <div className="col-md-4 col-12 menu">
                            <MenuList list={HEADER_MENU}/>
                        </div>

                        <div className="col-md-2 col-7 login-area">
                            <Profile name={name}/>
                        </div>

                    </div>

                </div>
            </div>
        </header>
    );
};

export default header;
