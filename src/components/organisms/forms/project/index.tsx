import React from 'react';
import Typography from "../../../atoms/typography";
import {TypographyType} from "../../../../model/enums/typography-type";
import {Input, Textarea} from "../../../atoms/inputs";
import styles from "./ProjectCreate.module.scss";

type Props = {};
// TODO i18n
const ProjectForm = (props: Props) => {
    return (
        <div className="project-detail">
            <div className="project-title">
                <Typography variant={TypographyType.H4}>Proje Adı</Typography>
                <Input type="text" className={styles.projectName} placeholder={"Lütfen projenizin adını yazınız"}/>
            </div>
            <div className="project-description">
                <Typography variant={TypographyType.H4}>Açıklama</Typography>
                <Textarea rows={8} placeholder={"Proje ile ilgili detayları bu alana yazabilirsiniz."}>
                </Textarea>
            </div>

        </div>
    );
};
export default ProjectForm;
