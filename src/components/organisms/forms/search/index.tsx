import React from "react";
import {IconGenerator} from "../../../atoms/icons";
import {IconType} from "../../../../model/enums/icon-type";
import {Input} from "../../../atoms/inputs";
import {useForm} from "react-hook-form";


interface Props {
    placeHolder?: string,
    onSearch: (value: string) => void
}

type SearchForm = {
    search: string
}
// TODO i18n
const search = React.memo(({placeHolder, onSearch}: Props) => {

    const {register, handleSubmit} = useForm<SearchForm>();

    const onSearchHandler = ({search}: SearchForm) => onSearch(search);

    return (
        <form className="navbar-search" onSubmit={handleSubmit(onSearchHandler)}>
            <Input name="search" className="search-query span3" placeholder={placeHolder} ref={register}/>
            <div className="icon-search">
                <IconGenerator icon={IconType.SEARCH}/>
            </div>
        </form>
    )
});


export default search;
