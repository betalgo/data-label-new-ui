import React from "react";
import {Input} from "../../../atoms/inputs/index";
import Link from "../../../atoms/link";
import Typography from "../../../atoms/typography";
import {TypographyType} from "../../../../model/enums/typography-type";
import Button from "../../../atoms/button";
import {useForm} from 'react-hook-form';
import {RegisterRequest} from "../../../../model/types/entities/user";
import {Checkbox} from "../../../atoms/inputs";

interface Props {
    onSubmit: (request: RegisterRequest) => void
}

// TODO i18n
const Register = React.memo(({onSubmit}: Props) => {

    const {register, handleSubmit} = useForm<RegisterRequest>();

    return (
        <div className="container">
            <div className="login-inner">
                <div className="login">

                    <Typography variant={TypographyType.TITLE}>ÜYE OL</Typography>

                    <form onSubmit={handleSubmit(onSubmit)}>

                        <Input name="username"
                               placeholder={"Kullanıcı Adınız veya E-Posta Adresiniz"}
                               inputType="border" ref={register}/>

                        <Input name="password"
                               type="password"
                               placeholder={"Şifreniz"} ref={register}
                               inputType="border"/>

                        <Input name="password"
                               type="password"
                               placeholder={"Şifrenizi Doğrulayın"} ref={register}
                               inputType="border"/>


                        <Input name="name"
                               placeholder={"Adınız"}
                               inputType="border" ref={register}/>

                        <Input name="surname"
                               placeholder={"Soyadınız"}
                               inputType="border" ref={register}/>

                        <Input name="email"
                               placeholder={"E-Posta Adresiniz"}
                               inputType="border" ref={register}/>

                        <Input name="phone"
                               placeholder={"Telefon numaranız"}
                               inputType="border" ref={register}/>

                        <Checkbox label={"Kullanıcı Sözleşmesini okudum ve kabul ediyorum."} name="contract"/>

                        {/*<Link to={"/"} className="password-forget" value={"Zaten üye misiniz? Giriş yapın"}/>*/}

                        <div className="button-area">
                            <Button type="submit" value="Kayıt Ol" className="project-button"/>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    );
});

export default Register;
