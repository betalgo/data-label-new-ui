import React from "react";
import {Input} from "../../../atoms/inputs/index";
import Link from "../../../atoms/link";
import Typography from "../../../atoms/typography";
import {TypographyType} from "../../../../model/enums/typography-type";
import Button from "../../../atoms/button";
import {useForm} from 'react-hook-form';
import {LoginRequest} from "../../../../model/types/entities/user";

interface Props {
    onSubmit: (request: LoginRequest) => void
}

// TODO i18n
const Login = React.memo(({onSubmit}: Props) => {

    const {register, handleSubmit} = useForm<LoginRequest>();

    return (
        <div className="container">
            <div className="login-inner">
                <div className="login">

                    <Typography variant={TypographyType.TITLE}>GİRİŞ YAP</Typography>

                    <form onSubmit={handleSubmit(onSubmit)}>

                        <Input name="username"
                               placeholder={"Kullanıcı Adınız veya E-Posta Adresiniz"}
                               inputType="border" ref={register}/>

                        <Input name="password"
                               type="password"
                               placeholder={"Şifreniz"} ref={register}
                               inputType="border"/>


                        {/*<Link to={"/"} className="password-forget" value={"Şifremi Unuttum"}/>*/}

                        <div className="button-area">
                            <Button  value="Giriş Yap" className="project-button"/>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    );
});

export default Login;
