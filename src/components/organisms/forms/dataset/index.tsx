import React from 'react';
import {FileUpload, Input, SelectBox} from '../../../atoms/inputs';
import {useForm} from "react-hook-form";
import {BaseDatasetUI} from "../../../../model/types/entities/dataset";
import Button from "../../../atoms/button";

type Props = {
    title?: string,
    onSubmit: (values: BaseDatasetUI) => void
};
// TODO i18n
const DatasetForm = React.memo(({title = "Yeni Veri Seti Yükle", onSubmit, ...props}: Props) => {

    const {register, handleSubmit} = useForm<BaseDatasetUI>();

    return (
        <div className="data-set-area">
            <form onSubmit={handleSubmit(onSubmit)}>

                <h5>{title}</h5>
                <div className="date-set-input">

                    <div className="form-row">
                        <Input name="name" inputType="compact" label="Veri Seti İsmi" ref={register}/>

                        <Input name="description" inputType="compact" label="Aciklama" ref={register}/>

                        <SelectBox name="contentType" options={[{label: "Text", value: "Text"}]} label={"İçerik Türü"}
                                   ref={register}/>

                        <FileUpload/>
                    </div>

                </div>

                <div className="button-area">
                    <Button type="submit" value="Oluştur" className="project-button"/>
                </div>
            </form>

        </div>
    );
});
export default DatasetForm;
