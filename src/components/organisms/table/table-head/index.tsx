import React from 'react';

type Props = {
    titles: Array<string>
};

const tableHead = ({titles}: Props) => {
    return (
        <thead>
        <tr>
            {titles.map(t => <th scope="col">{t}</th>)}
        </tr>
        </thead>
    );
};
export default tableHead;
