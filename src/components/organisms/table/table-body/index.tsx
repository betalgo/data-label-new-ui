import React from 'react';
import TableRow from "../../../molecules/row/table-row-item";

type Props = {
    rows: Array<any>
};

const tableBody = (props: Props) => {
    return (
        <tbody>
        <tr>
            {
                props.rows.map(r => <TableRow>{r}</TableRow>)
            }
        </tr>
        </tbody>
    );
};
export default tableBody;
