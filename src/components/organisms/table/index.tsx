import React from 'react';
import TableHead from "./table-head";
import TableBody from "./table-body";

type Props = {
    titles: Array<string>,
    rows: Array<any>
};

const datasetList = (props: Props) => {
    return (
        <div className="table-responsive">
            <table className="table table-labelling">

                <TableHead titles={props.titles}/>
                <TableBody rows={props.rows}/>

            </table>
        </div>
    );
};
export default datasetList;
