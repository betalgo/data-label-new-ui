import React from 'react';
import {RadioOrCheckbox} from "../../../model/types/ui/RadioOrCheckbox";
import {Radiobutton} from '../../atoms/inputs';
import Typography from "../../atoms/typography";
import {TypographyType} from "../../../model/enums/typography-type";

type Props = {
    list: Array<RadioOrCheckbox>,
    disabled?: boolean,
    title: string
};

const RadioGroup = ({list, disabled = false, title, ...props}: Props) => {
    return (
        <>
            <Typography variant={TypographyType.H4}>{title}</Typography>

            {
                list.map(r => {
                    return <><Radiobutton key={r.value} {...r} disabled={disabled}/><br/></>
                })
            }
        </>
    );
};
export default RadioGroup;
