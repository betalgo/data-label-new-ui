import React from 'react';
import {RadioOrCheckbox} from "../../../model/types/ui/RadioOrCheckbox";
import {Checkbox} from '../../atoms/inputs';
import Typography from "../../atoms/typography";
import {TypographyType} from "../../../model/enums/typography-type";

type Props = {
    list: Array<RadioOrCheckbox>,
    disabled?: boolean,
    title: string
};

const CheckboxGroup = React.memo(({list, title, disabled = false, ...props}: Props) => {
    return (
        <>
            <Typography variant={TypographyType.H4}>{title}</Typography>

            {
                list.map(r => {
                    return <><Checkbox key={r.value} {...r} disabled={disabled}/><br/></>
                })
            }
        </>
    );
});
export default CheckboxGroup;
