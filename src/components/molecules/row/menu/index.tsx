import React from "react";
import {IconGenerator} from "../../../atoms/icons";
import {Menu} from "../../../../model/types/ui/Menu";

interface Props extends Menu{
}

const menu = ({icon, to = "#", value}: Props) => {

    // TODO change a via LINK atom after implement router
    return (
        <a href={to}>
            <IconGenerator icon={icon}/>{value}
        </a>
    )
};

export default menu;
