import React from 'react';
import {AuxProps} from "../../../../model/interfaces/AuxProps";

const tableHeadRow = (props: AuxProps) => <th>{props.children}</th>
export default tableHeadRow;
