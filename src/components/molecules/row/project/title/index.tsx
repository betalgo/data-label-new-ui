// @flow
import * as React from 'react';
import {CircleIconType} from "../../../../../model/enums/circle-icon-type";
import {CircleIcon} from "../../../../atoms/icons";

type Props = {
    title: string,
    icon: CircleIconType
};
export default React.memo((props: Props) => <div className="card-head">
    <p>
        <CircleIcon icon={props.icon}/> {props.title}
    </p>
</div>);
