import React from 'react';
import CardTitle from "./title";
import CardDetail from "./detail";
import CardFooter from "./footer";
import {ContentType} from "../../../../model/enums/content-type";
import {ContentTypeMapping} from "../../../../common/object-mapper/ContentTypeMapping";
import {CircleIconType} from "../../../../model/enums/circle-icon-type";

type Props = {
    contentType: ContentType,
    method: string,
    progressBar: number,
    name: string,
    count: number
};
const projectItem = ({method, name, count, progressBar, contentType, ...props}: Props) => {

    const circleIcon: CircleIconType = ContentTypeMapping.convertContentTypeToIcon(contentType);
    return (
        <div className="card">
            <div className="card-body">

                <CardTitle title={method} icon={circleIcon}/>

                <CardDetail name={name} progressBar={progressBar}/>

                <CardFooter count={count}/>

            </div>
        </div>
    );
};

export default projectItem;
