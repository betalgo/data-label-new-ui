import React from 'react';

type Props = {
    count: number,
    time?: string
};
// TODO i18n
const footer = React.memo((props: Props) => {
    return (
        <div className="card-foot">
            <p>{props.time}</p>
            <div className="mute-text">
                <p>İçerik Miktarı: {props.count}</p>
            </div>
        </div>
    );
});
export default footer;
