// @flow
import * as React from 'react';
import ProgressBar from "../../../../atoms/progress-bar";
import Typography from "../../../../atoms/typography";
import {TypographyType} from "../../../../../model/enums/typography-type";

type Props = {
    name: string,
    progressBar: number
};
const cardDetail = React.memo((props: Props) => {
    return (
        <div className="card-detail">
            <Typography variant={TypographyType.H3}>{props.name}</Typography>
            <ProgressBar completed={props.progressBar}/>
        </div>
    );
});

export default cardDetail;
