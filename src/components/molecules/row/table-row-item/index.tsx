import React from 'react';
import {AuxProps} from "../../../../model/interfaces/AuxProps";

const tableRow = (props: AuxProps) => <td>{props.children}</td>

export default tableRow;
