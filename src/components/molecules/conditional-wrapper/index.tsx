import React from 'react';
import {AuxProps} from "../../../model/interfaces/AuxProps";

type Props = {
    condition: boolean,
    wrapper: (children: React.ReactNode) => any
} & AuxProps;

const ConditionalWrapper = ({condition, children, wrapper}: Props) => {
    return condition ? wrapper(children) : children;
};
export default ConditionalWrapper;
