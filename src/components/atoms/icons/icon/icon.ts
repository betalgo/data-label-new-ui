import {ReactComponent as ProjectIcon} from "../../../../assets/icon/project.svg";
import {ReactComponent as DatasetIcon} from "../../../../assets/icon/dataset.svg";
import {ReactComponent as SearchIcon} from "../../../../assets/icon/search.svg";
import {ReactComponent as AppLogo} from "../../../../assets/icon/logo.svg";

export {ProjectIcon, DatasetIcon, SearchIcon, AppLogo};
