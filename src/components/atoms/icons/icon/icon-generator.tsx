import React from "react";
import {IconType} from "../../../../model/enums/icon-type";
import {AppLogo, DatasetIcon, ProjectIcon, SearchIcon} from "./icon";

interface Props {
    icon: IconType
}

const iconGenerator = (props: Props) => {

    switch (props.icon) {
        case IconType.PROJECT:
            return <ProjectIcon/>;
        case IconType.DATASET:
            return <DatasetIcon/>;
        case IconType.SEARCH:
            return <SearchIcon/>;
        case IconType.APP_LOGO:
            return <AppLogo style={{verticalAlign: "middle", borderStyle: "none"}}/>;
        case IconType.TRASH:
            return <i className="icon-trash-icon"></i>;
        case IconType.PLUS:
            return <i className="icon-plus-icon"></i>;
        default:
            return <></>;
    }
};

export default iconGenerator;
