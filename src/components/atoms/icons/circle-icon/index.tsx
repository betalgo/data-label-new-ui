import React from 'react';
import {CircleIconType} from "../../../../model/enums/circle-icon-type";

type Props = {
    icon: CircleIconType
};

 const circleIcon = (props: Props) => {

    switch (props.icon) {
        case CircleIconType.DOCUMENT:
            return <i className="icon-image-icon"/>;
        case CircleIconType.IMAGE:
            return <i className="icon-document-icon"/>;
        case CircleIconType.VIDEO:
            return <i className="icon-document-play"/>;
        default:
            return <></>;
    }
};
export default circleIcon;
