import React from "react";
import {TypographyType} from "../../../model/enums/typography-type";
import {AuxProps} from "../../../model/interfaces/AuxProps";

interface Props extends AuxProps{
    variant?: TypographyType
}


const typography = (props: Props) => {
    switch (props.variant) {
        case TypographyType.H1:
            return <h1>{props.children}</h1>;
        case TypographyType.H2:
        case TypographyType.TITLE:
            return <h2>{props.children}</h2>;
        case TypographyType.H3:
            return <h3>{props.children}</h3>;
        case TypographyType.H4:
            return <h4>{props.children}</h4>;
        case TypographyType.H5:
            return <h5>{props.children}</h5>;
        case TypographyType.H6:
            return <h6>{props.children}</h6>;
        case TypographyType.HEADLINE:
            return <span>{props.children}</span>;
        case TypographyType.SUBHEADING:
            return <span>{props.children}</span>;
        case TypographyType.CAPTION:
            return <span>{props.children}</span>;
        default:
            return <span>{props.children}</span>;
    }
}

typography.defaultProps = {
    variant: TypographyType.SUBHEADING
}

export default typography;
