import React from "react";
import {Link} from 'react-router-dom';

interface Props {
    value: string,
    to?: string,
    className?: string
}

const link = ({className, value, to= "#"}: Props) => {
    return (
        <Link to={to} className={className}>
            {value}
        </Link>
    )
};


export default link;
