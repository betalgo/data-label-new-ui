import React from "react";
import Input from "../input";
import {RadioOrCheckbox} from "../../../../model/types/ui/RadioOrCheckbox";

const checkbox = React.memo(({label, name, value, checked = false, disabled = false}: RadioOrCheckbox) => {
    return (
        <label>
            <Input type="checkbox" checked={checked} name={name} disabled={disabled} value={value}/>
            {label}
        </label>
    );
});
export default checkbox;
