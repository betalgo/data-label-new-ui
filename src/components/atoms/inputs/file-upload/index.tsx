import React from 'react';
import {Input} from "../index";

type Props = {
    label?: string
};
// TODO i18n
const fileUpload = React.memo(({label = "Dosya Yükleme"}: Props) => {
    return (
        <div className="col-6 col-md-3 classic-input mb-2">
            <div className="custom-upload">
                <span>{label}</span>
                <label htmlFor="upload-file" className="customform-control upload">Dosya Seçiniz</label>
                <Input type='file' placeholder="Browse computer" id="upload-file"/>
                <span id='val'></span>
            </div>
        </div>
    );
});
export default fileUpload;
