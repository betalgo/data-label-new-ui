import React, {TextareaHTMLAttributes} from 'react';
import styles from "./Textarea.module.scss";

type Props = {} & TextareaHTMLAttributes<HTMLTextAreaElement>;

const textarea = React.memo((props: Props) => {
    return (
        <textarea {...props} className={styles.borderless}>{props.children}</textarea>
    );
});
export default textarea;
