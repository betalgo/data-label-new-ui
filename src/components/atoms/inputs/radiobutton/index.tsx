import React from "react";
import Input from "../input";
import {RadioOrCheckbox} from "../../../../model/types/ui/RadioOrCheckbox";

const radiobutton = React.memo(({name, value, checked = false, disabled = false, label}: RadioOrCheckbox) => {
    return (
        <>
            <Input type="radio" name={name} value={value} disabled={disabled} checked={checked}/>
            <label>{label}</label>
        </>
    );
});

export default radiobutton;
