import React, {InputHTMLAttributes} from "react";
import styles from "./Input.module.scss";
import classNames from "classnames";

interface Props extends InputHTMLAttributes<HTMLInputElement> {
    type?: string,
    label?: string,
    inputType?: "border" | "compact" | "standard"
}

const input = React.memo(React.forwardRef<HTMLInputElement, Props>(
    ({type = "text", label, inputType = "standard", ...props}: Props, forwardedRef) => {

        const inputClassName = classNames(
            props.className,
            {[styles.borderedInput]: inputType === "border"},
            {[styles.borderlessInput]: inputType === "standard"},
            {"form-control": inputType === "compact"}
        );

        const inputComponent = <input {...props} type={type} ref={forwardedRef} className={inputClassName}/>;

        switch (inputType) {
            case "border":
                return <div className="classic-input">{inputComponent}</div>;
            case "compact":
                return <div className="col-6 col-md-3 classic-input mb-2">
                    <span>{label}</span>
                    {inputComponent}
                </div>;
            case "standard":
            default:
                return inputComponent;

        }

    }
));

export default input;

