import React, {InputHTMLAttributes} from 'react';
import {DropdownOption} from "../../../../model/types/ui/DropdownOption";

type Props = {
    options: Array<DropdownOption>,
    label: string,
    name: string
} & InputHTMLAttributes<HTMLSelectElement>;

function createOption(option: DropdownOption) {
    return <option key={option.value} value={option.value}>{option.label}</option>
}

const dropdown = React.memo(React.forwardRef<HTMLSelectElement, Props>(
    ({label, options, name, ...props}: Props, forwardedRef) => {
        return (
            <div className="col-6 col-md-3 classic-input mb-2">
                <div className="select-area">
                    <span>{label}</span>
                    <select name={name} className="classic-select" ref={forwardedRef}>
                        <option value="">Seçiniz</option>
                        {options.map(o => createOption(o))}
                    </select>
                </div>
            </div>
        );
    }));
export default dropdown;
