import React, {ButtonHTMLAttributes} from "react";
import styles from "./Button.module.scss";
import classNames from "classnames";

interface Props extends ButtonHTMLAttributes<HTMLButtonElement> {
    value: string,
    className?: string
}

const button = React.memo(({value, className = "project-index", ...props}: Props) => {

    const buttonClass = classNames(styles.button, className);

    return (
        <button {...props} className={buttonClass}>{value}</button>
    );
});

export default button;
