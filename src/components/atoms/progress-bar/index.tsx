import React from "react";

interface Props {
    completed: number
}

const ProgressBar = ({completed}: Props) => {

    return (
        <div className="progress">
            <div className="progress-bar" role="progressbar" style={{width: `${completed}%`}}></div>
        </div>
    );
};

export default ProgressBar;
