// region DB Model
export type UserDBModel = {}
export type UserUIModel = {}

// endregion

// region API Request Response

export type LoginRequest = {
    username: string,
    password: string
}

export type RegisterRequest = {
    username: string,
    password: string,
    name: string,
    surname: string,
    email: string,
    phone: string,
    contract: boolean
}
// endregion
