// region DB MODEL

import {ContentTypeDBModel} from "./content-type";

export type DatasetDBModel = {}

export type BaseDatasetUI = {
    name: string,
    description: string,
    contentType: ContentTypeDBModel
}

export type DatasetUIModel = {} & BaseDatasetUI;

// endregion
