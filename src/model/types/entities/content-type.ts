export type ContentTypeDBModel = {
    name: string,
    format: string,
    contentTypeCode: string
}

export type ContentTypeUIModel = {}
