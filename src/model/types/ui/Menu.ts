import {IconType} from "../../enums/icon-type";

export type Menu = {
    icon: IconType,
    to?: string,
    value: string
}
