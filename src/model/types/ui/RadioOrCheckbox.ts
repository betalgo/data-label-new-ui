export type RadioOrCheckbox = {
    value?: string,
    checked?: boolean,
    label: string,
    name: string,
    disabled?: boolean
}
