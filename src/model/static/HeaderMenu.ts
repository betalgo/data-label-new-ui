import {IconType} from "../enums/icon-type";
import { Menu } from "../types/ui/Menu";

export const HEADER_MENU: Array<Menu> = [{
    to: "/projects",
    value: "Projeler",
    icon: IconType.PROJECT
}, {
    to: "/datasets",
    value: "Veri Setleri",
    icon: IconType.DATASET
}]
