export enum ContentType {
    IMAGE = "IMAGE",
    TEXT = "TEXT",
    VOICE = "VOICE",
}
