export enum IconType {
    PROJECT = "PROJECT",
    DATASET = "DATASET",
    APP_LOGO = "APP_LOGO",
    SEARCH = "SEARCH",
    TRASH = "TRASH",
    PLUS = "PLUS"
}
