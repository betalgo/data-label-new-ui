import {ContentType} from "../../model/enums/content-type";
import {CircleIconType} from "../../model/enums/circle-icon-type";

export class ContentTypeMapping {

    public static convertContentTypeToIcon(type: ContentType) {
        switch (type) {
            case ContentType.IMAGE:
                return CircleIconType.IMAGE;
            case ContentType.TEXT:
                return CircleIconType.DOCUMENT;
            case ContentType.VOICE:
                return CircleIconType.VIDEO;
            default:
                return CircleIconType.DOCUMENT;
        }
    }
};
