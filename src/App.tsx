import React from 'react';
import "./assets/styles/datalabelling.css";

import Register from "./pages/authentication/Register";
import Login from "./pages/authentication/Login";
import Project from "./pages/project/Project";
import ProjectCreate from "./pages/project/create";
import Step2 from './pages/project/create/step2';

function App() {
    return (
        <div className="wrappers">

            <Step2 />

        </div>
    );
}

export default App;
