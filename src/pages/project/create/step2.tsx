import React from 'react';
import EditorTemplate from "../../../components/templates/editor-template/EditorTemplate";
import Typography from "../../../components/atoms/typography";
import Table from "../../../components/organisms/table";
import DatasetForm from "../../../components/organisms/forms/dataset";
import {TypographyType} from "../../../model/enums/typography-type";
import ProjectCreateTemplate from "../../../components/templates/project-create-template/ProjectCreateTemplate";


type Props = {};
type State = {};

class Step2 extends React.Component<Props, State> {
    render() {
        return (
            <ProjectCreateTemplate name={"Fatih"}>

                <>
                    <div className="label-text-head">
                        <h4>Yeni Proje</h4>
                        <input type="radio" name="text" value=""/>
                        <label>Etiketleme Modeli</label>

                        <input type="radio" name="text" value=""/>
                        <label>Veri Seti Oluştur veya Ekle</label>
                    </div>

                    <div className="data-set-area">
                        <DatasetForm onSubmit={(values => console.log(values))}/>
                    </div>

                    <div className="table-area">
                        <Typography variant={TypographyType.H5}>Eklenen Veri setleri</Typography>
                        <Table titles={["title", "description"]} rows={["a", "b"]}/>
                    </div>


                    <div className="data-type-footer">
                    {/*    buttons  */}
                    </div>

                </>

            </ProjectCreateTemplate>
        );
    };
};

export default Step2;
