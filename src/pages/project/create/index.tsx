import React from 'react';
import EditorTemplate from "../../../components/templates/editor-template/EditorTemplate";
import ProjectForm from "../../../components/organisms/forms/project";


type Props = {};
type State = {};

class ProjectCreate extends React.Component<Props, State> {
    render() {

        const top = (<>
            <h4>Yeni Proje</h4>
            <input type="radio" name="text" value=""/>
            <label>Etiketleme Modeli</label>

            <input type="radio" name="text" value=""/>
            <label>Veri Seti Oluştur veya Ekle</label>
        </>);

        const right = (<div className="project-detail">
            <div className="project-title">
                {/* RADIO or CHECKBOX */}
            </div>
            <div className="project-description">
                {/* RADIO or CHECKBOX */}
            </div>
        </div>)

        return (
            <EditorTemplate name={"Fatih"} top={top} left={<ProjectForm/>} right={right}/>
        );
    };
};

export default ProjectCreate;
