import React from 'react';
import BaseTemplate from "../../components/templates/base-template/BaseTemplate";
import ProgressBar from "../../components/atoms/progress-bar";
import ProjectList from "../../components/organisms/list/project";
import Button from "../../components/atoms/button";
import {ContentType} from "../../model/enums/content-type";
import ProjectTemplate from "../../components/templates/project-template/ProjectTemplate";

type Props = {};
type State = {
    projects: Array<any>
};

// TODO i18n
class Project extends React.Component<Props, State> {


    state = {
        projects: [{
            contentType: ContentType.VOICE,
            method: "Classification",
            name: "Yaani Arama Motoru",
            count: 1230,
            progressBar: 63
        }, {
            contentType: ContentType.IMAGE,
            method: "Classification",
            name: "Turkgen Object Detection Labeling",
            count: 1230,
            progressBar: 30
        }, {
            contentType: ContentType.TEXT,
            method: "Classification",
            name: "GYK Widget Classification",
            count: 1230,
            progressBar: 12
        }]
    }

    render() {
        return (
            <ProjectTemplate name={"Ramazan"}>

                <div className="container">
                    <ProjectList projects={this.state.projects}/>

                    <div className="button-area">
                        <Button className="project-button" value="Yeni Proje"/>
                    </div>

                </div>


            </ProjectTemplate>
        );
    };
};

export default Project;
