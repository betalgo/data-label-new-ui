import React from "react";
import LoginForm from "../../components/organisms/forms/login";
import LoginTemplate from "../../components/templates/login-template/LoginTemplate";
import {LoginRequest} from "../../model/types/entities/user";
import BaseTemplate from "../../components/templates/base-template/BaseTemplate";

class Login extends React.Component {

    onSubmitHandler = (request: LoginRequest) => {
        console.log(request);
    }


    render() {
        return (
            <BaseTemplate name={"Ramazan"}>

                <LoginForm onSubmit={this.onSubmitHandler}/>

            </BaseTemplate>
        );
    }


}

export default Login;
