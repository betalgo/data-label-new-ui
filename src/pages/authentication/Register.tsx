import React from 'react';
import LoginTemplate from '../../components/templates/login-template/LoginTemplate';
import {RegisterForm} from "../../components/organisms/forms";
import {RegisterRequest} from "../../model/types/entities/user";

class Register extends React.Component {

    onSubmitHandler = (request: RegisterRequest) => {
        console.log(request);
    }

    render() {
        return (
            <LoginTemplate>

                <RegisterForm onSubmit={this.onSubmitHandler}/>

            </LoginTemplate>
        );
    };
};

export default Register;
